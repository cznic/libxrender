// Copyright 2024 The libXrender-go Authors. All rights reserved.
// Use of the source code is governed by a BSD-style
// license that can be found in the LICENSE file.

//go:generate go run generator.go

// Package libxrender is a ccgo/v4 version of libXrender.a, an image
// composition library extension of the Core X11 client protocol.
package libxrender // import "modernc.org/libXrender"
